﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FileHelpers;

namespace SpringSprintsResults
{
    [DelimitedRecord(",")]
    public class Result
    {
        public string Place;
        public string Time;
        public string Meters;
        public string Name;
        public string Pace;
        public string Id;
        public string Bib;
        public string Class;

        [FieldOptional, FieldNullValue(0.0)] public Double Points;

        public TimeSpan Span()
        {
            TimeSpan ts = new TimeSpan(9,9,9);
            var d = 0;
            var h = 0;
            var m = 0;
            var s = 0;
            var ms = 0;

            var sp = Time.Split(':');


            var c = sp.Count();
            if (c==1) //no hours or minutes
            {
                var sec = decimal.Parse(sp.First());
                s = (int)Math.Floor(sec);
                sec = sec - s;
                ms = (int)Math.Floor(sec * 1000);
                ts = new TimeSpan(d,h,m,s,ms);
            }

            if (c==2) //no hours but minutes
            {
                m = int.Parse(sp.First());
                var sec = decimal.Parse(sp.Last());
                s = (int)Math.Floor(sec);
                sec = sec - s;
                ms = (int)Math.Floor(sec * 1000);
                ts = new TimeSpan(d, h, m, s, ms);
            }

            if (c == 3) //hours and minutes
            {
                h = int.Parse(sp.First());
                m = int.Parse(sp[1]);
                var sec = decimal.Parse(sp.Last());
                s = (int)Math.Floor(sec);
                sec = sec - s;
                ms = (int)Math.Floor(sec * 1000);
                ts = new TimeSpan(d, h, m, s, ms);
            }

            return ts;
        }

        public string Team()
        {
            var s = Name.Split(')').First();
            var s1 = s.Split('(')
                .Last();

            return s1;

        }
    }
}
