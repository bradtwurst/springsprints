﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpringSprintsResults
{
    class Program
    {
        static void Main(string[] args)
        {

            var teamPoints = new Dictionary<string, decimal>();

            var races = new Dictionary<int, List<string>>();

            var files = @"C:\Users\bradt\Documents\Concept2\Venue Race\Race Results";
            foreach (var file in System.IO.Directory.EnumerateFiles(files, "* Results.txt"))
            {
                var fname = System.IO.Path.GetFileName(file);
                if (fname.StartsWith("E") && fname.Substring(2,1) == "F")
                {
                    var c = fname.Substring(1, 1);
                    int i = 0;
                    if (int.TryParse(c,out i))
                    {
                        if (i >= 1 && i <= 8)
                        {
                            var racenbr = i;
                            var txt = System.IO.File.ReadAllLines(file);
                            bool capture = false;

                            foreach (var s in txt)
                            {
                                if (capture && string.IsNullOrWhiteSpace(s))
                                {
                                    capture = false;
                                }

                                if (capture)
                                {
                                    List<string> l;
                                    if (!races.TryGetValue(racenbr, out l))
                                    {
                                        races[racenbr] = new List<string>();
                                        l = races[racenbr];
                                    }
                                    l.Add(s);
                                }

                                if (!capture && s.StartsWith(@"Place,Time Rowed,Meters Rowed"))
                                {
                                    capture = true;
                                }

                            }
                        }
                    }
                }
            }



            foreach (var racekvp in races)
            {
                var evnt = racekvp.Key;
                var rowers = racekvp.Value;
                var rowString = string.Join(Environment.NewLine, rowers);
                var en = new FileHelpers.DelimitedFileEngine<Result>();
                var result = en.ReadStringAsList(rowString);


                var nondnf = result.Where(
                    x => x.Span()
                             .Ticks != 0 && x.Meters !=  "0").OrderBy(x=>x.Span().Ticks)
                    .ToList();

                var places = nondnf.GroupBy(x => x.Span())
                    .ToList();

                var cntr = 1;
                var placeCntr = 1;
                var pntCntr = 10;

                if (evnt == 3 || evnt == 4)
                {
                    pntCntr = 0;
                }

                foreach (var place in places)
                {
                    placeCntr = cntr;

                    var pointTtls = 0;
                    var cc = place.Count();
                    for (int i = 0; i < cc; i++)
                    {
                        pointTtls += pntCntr;
                        pntCntr -= 1;
                        if (pntCntr < 0)
                        {
                            pntCntr = 0;
                        }
                    }

                    var pts = Math.Round( ((decimal)pointTtls) / cc, 2);

                    foreach (var result1 in place)
                    {
                        result1.Points = (double)pts;
                        if (cc > 1)
                        {
                            result1.Place = "T" + placeCntr.ToString();

                        }
                        else
                        {
                            result1.Place = placeCntr.ToString();
                            
                        }
                        cntr += 1;

                        if (evnt != 3 && evnt != 4)
                        {
                            var t = result1.Team();
                            decimal dp;
                            if (!teamPoints.TryGetValue(t, out dp))
                            {
                                dp = 0;
                            }
                            dp += pts;
                            teamPoints[t] = dp;
                            
                        }
                    }
                }

                var fn = System.IO.Path.Combine(files, "Results " + evnt + ".csv");
                en.HeaderText = "Place, Time, Meters, Name, Pace, Id, Bib, Class, Points";
                en.WriteFile(fn, nondnf);

                var txtList = teamPoints.OrderByDescending(x=>x.Value).Select(x => string.Format("{0}: {1}", x.Key, x.Value));
                System.IO.File.WriteAllLines(System.IO.Path.Combine(files,"TeamPoints.txt"), txtList);

            }



            var xxx = 123;
        }
    }
}
