﻿Imports FileHelpers


'EventNumber	Rank	Team	LastName	FirstInitial

<DelimitedRecord(",")>
<IgnoreFirst()>
Public Class ErgReport

    Public Shared HeaderText = "Lane, Race, Heat, Team, LastName, FirstInitial"


    Public Lane As String

    Public EventNumber As Integer

    Public Heat As String

    Public Team As String

    Public LastName As String

    Public FirstInitial As String


End Class
