﻿Imports FileHelpers


'EventNumber	Rank	Team	LastName	FirstInitial

<DelimitedRecord(",")>
<IgnoreFirst()>
Public Class Roster

    Public Shared HeaderText = "EventNumber, Rank, Team, LastName, FirstInitial, RowerNbr, Race, Heat, Lane"

    Public EventNumber As Integer

    Public Rank As Integer

    Public Team As String

    Public LastName As String

    Public FirstInitial As String

    <FieldOptional()>
    Public BibNumber As String

    <FieldOptional()>
    Public RaceEntry As String

    <FieldOptional()>
    Public Heat As String

    <FieldOptional()>
    Public Lane As String

End Class

