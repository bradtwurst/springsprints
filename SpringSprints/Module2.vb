﻿Module Module2
    Public Sub WriteTestRace(raceDir As String)
        Dim raceHeader = "RACE|108|0|TestRace|15|0|0|1|500|120|30"
        Dim raceData = raceHeader

        For laneCntr = 1 To 30
            Dim raceEntry = String.Format("|(TEST) Lane {0}|{0}|E0|USA|", laneCntr)
            raceData &= raceEntry
        Next

        Dim outName = IO.Path.Combine(raceDir, "TEST.rac")
        Dim outData = raceData.Split("|")
        IO.File.WriteAllLines(outName, outData)

    End Sub
End Module
