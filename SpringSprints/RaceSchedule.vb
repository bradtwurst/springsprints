﻿Imports FileHelpers


'EventNumber	Rank	Team	LastName	FirstInitial

<DelimitedRecord(",")>
<IgnoreFirst()>
Public Class RaceSchedule

    Public Shared HeaderText = "Race, Event, Heat, RaceTime"


    Public RaceNumber As Integer

    Public EventName As String

    Public Heat As Integer

    Public RaceTime As String

End Class
