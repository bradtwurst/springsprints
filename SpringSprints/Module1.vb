﻿Imports FileHelpers

Module Module1

    Public _log As log4net.ILog = log4net.LogManager.GetLogger(GetType(Module1))

    Sub Main()
        log4net.Config.XmlConfigurator.ConfigureAndWatch(New IO.FileInfo(".\\log4net.config.xml"))
        Dim filename = "C:\Spring_Sprints\roster.csv"
        Dim outputFileName = "C:\Spring_Sprints\rosterOut.csv"
        Dim weighInFileName = "C:\Spring_Sprints\weighIn.csv"
        Dim teamReports = "C:\Spring_Sprints\Team{0}.csv"
        Dim raceReports = "C:\Spring_Sprints\Race {0}_{1}.csv"
        Dim teamCountFilename = "C:\Spring_Sprints\TeamCount.csv"
        Dim scheduleFilename = "C:\Spring_Sprints\RaceSchedule.csv"
        Dim ergReports = "C:\Spring_Sprints\Lane{0}.csv"


        Dim racedir = "C:\Spring_Sprints"
        Dim maxErg = 30
        Dim bibCntr = 0

        If maxErg Mod 2 <> 0 Then
            _log.Error("max erg {0} is not an even number")
            Console.ReadLine()
            Exit Sub
        End If

        Dim halfMaxErg = maxErg \ 2


        Dim engine As New FileHelperEngine(Of Roster)()
        engine.HeaderText = Roster.HeaderText
        Dim rosterList = engine.ReadFileAsList(filename)

        Dim rosterOutList As New List(Of Roster)
        Dim schedule As New List(Of RaceSchedule)

        Dim events As New Dictionary(Of Integer, String) From
            {
                {1, "Novice Men - 1500m"},
                {2, "Novice Women - 1500m"},
                {3, "Coxswain Men - 500m (No team points)"},
                {4, "Coxswain Women - 500m (No team points)"},
                {5, "150# (155) Men - 2000m"},
                {6, "130# (135) Women - 2000m"},
                {7, "Open Men - 2000m"},
                {8, "Open Women - 2000m"}
            }

        Dim lwEvents As New List(Of String) From {
            5, 6}


        Dim startTime As DateTime = New DateTime(1, 1, 1, 9, 0, 0)
        Dim offsetTime As TimeSpan = New TimeSpan(0, 13, 0)
        Dim raceTime = startTime

        Dim lengths As New Dictionary(Of Integer, Integer) From
            {
                {1, 1500},
                {2, 1500},
                {3, 500},
                {4, 500},
                {5, 2000},
                {6, 2000},
                {7, 2000},
                {8, 2000}
            }

        Dim eventRosters = (From item In rosterList Group By item.EventNumber Into Group Order By EventNumber).ToList

        For Each eventRoster In eventRosters

            Dim rosterCount = eventRoster.Group.Count
            Dim flightCount = rosterCount \ maxErg + 1

            Dim rosterPerFlight = rosterCount \ flightCount + 1

            Dim flightCntr = 1
            Dim rosterCntr = 0
            Dim items = (From item In eventRoster.Group Order By item.Rank Descending, item.Team).ToList

            Dim cntr = New Func(Of Integer)(
                       Function()
                           rosterCntr += 1
                           Return rosterCntr
                       End Function)



            Dim groups = (From item In items
                          Let rcntr = cntr.Invoke
                          Let fcntr = (rcntr - 1) \ rosterPerFlight + 1
                          Select rcntr, fcntr, item
                          Group By fcntr Into Group
                          ).ToList

            For Each group In groups
                Dim groupList = group.Group.ToList.Select(Function(x) x.item).ToList
                Dim firstitem = groupList.First()
                For groupCntr = groupList.Count + 1 To maxErg
                    Dim tmpRoster = New Roster With {.EventNumber = firstitem.EventNumber, .FirstInitial = "", .LastName = "Empty", .Rank = -1, .Team = "EmptyLane"}
                    groupList.Insert(0, tmpRoster)
                Next
                If groupList.Count <> maxErg Then
                    _log.Error(String.Format("flight count {0} does not match erg count {1}", groupList.Count, maxErg))
                    Console.ReadLine()
                    Exit Sub
                End If

                Dim itemCntr = 0
                Dim itemList1 = New List(Of Roster)
                Dim itemList2 = New List(Of Roster)
                For Each item In groupList
                    itemCntr += 1
                    If itemCntr Mod 2 = 1 Then
                        itemList1.Add(item)
                    Else
                        itemList2.Add(item)
                    End If
                Next
                itemList2.Reverse()
                itemList1.AddRange(itemList2)


                Dim raceHeader = "RACE|108|0|{eventName}|{eventLength}|0|0|1|500|120|{maxErg}"
                Dim raceData = raceHeader
                raceData = raceData.Replace("{eventLength}", lengths.Item(eventRoster.EventNumber))
                raceData = raceData.Replace("{eventName}", String.Format("E{0}F{1}", firstitem.EventNumber, group.fcntr))
                raceData = raceData.Replace("{maxErg}", maxErg)


                Dim laneCntr = 0
                For Each item In itemList1
                    laneCntr += 1
                    bibCntr += 1
                    item.BibNumber = bibCntr
                    item.RaceEntry = String.Format("E{0}F{1}L{2}", firstitem.EventNumber, group.fcntr, laneCntr)
                    item.Heat = group.fcntr
                    item.Lane = laneCntr
                    rosterOutList.Add(item)

                    Dim raceEntry = String.Format("|({0}) {1} {2}|{3}|E{4}|USA|", item.Team, item.LastName, item.FirstInitial, item.BibNumber, firstitem.EventNumber)
                    raceData &= raceEntry
                Next

                Dim outName = IO.Path.Combine(racedir, String.Format("E{0}F{1}.rac", firstitem.EventNumber, group.fcntr))
                Dim outData = raceData.Split("|")
                IO.File.WriteAllLines(outName, outData)

                Dim s = New RaceSchedule With
                        {.EventName = events.Item(firstitem.EventNumber),
                         .Heat = group.fcntr,
                         .RaceNumber = firstitem.EventNumber,
                         .RaceTime = raceTime.ToShortTimeString}

                schedule.Add(s)

                raceTime = raceTime.Add(offsetTime)

            Next
        Next

        'Dim outputFileName = "C:\Spring_Sprints\rosterOut.csv"
        'Dim weighIn = "C:\Spring_Sprints\weighIn.csv"
        'Dim teamReports = "C:\Spring_Sprints\Team_{0}.csv"
        'Dim teamCount = "C:\Spring_Sprints\TeamCount.csv"
        'Dim ergReports = "C:\Spring_Sprints\Lane_{0}.csv"

        Dim lw = From i In rosterOutList Where lwEvents.Contains(i.EventNumber) And i.Team <> "EmptyLane"
                 Order By i.Team, i.EventNumber, i.LastName, i.FirstInitial
                 Select New WeighIn() With {
                 .EventName = events.Item(i.EventNumber),
                 .FirstInitial = i.FirstInitial,
                 .LastName = i.LastName,
                 .Team = i.Team}

        Dim e1 = New FileHelperEngine(Of WeighIn)()
        e1.HeaderText = WeighIn.HeaderText

        e1.WriteFile(weighInFileName, lw.ToList)

        Dim rost = From i In rosterOutList Order By i.Team, i.EventNumber, i.LastName, i.FirstInitial, i.LastName Group By key = i.Team Into Group Select Team = key, Roster = Group

        For Each team In rost
            engine.HeaderText = Roster.HeaderText
            engine.WriteFile(String.Format(teamReports, team.Team), team.Roster.ToList)
        Next

        Dim races = From i In rosterOutList Order By i.EventNumber, Integer.Parse(i.Heat), Integer.Parse(i.Lane) Group By key = i.EventNumber, key1 = i.Heat Into Group Select Evnt = key, Ht = key1, Roster = Group

        For Each team In races
            engine.HeaderText = Roster.HeaderText
            engine.WriteFile(String.Format(raceReports, team.Evnt, team.Ht), team.Roster.ToList)
        Next

        Dim count = From team In rost Where team.Team <> "EmptyLane" Select New TeamCount With {.Count = team.Roster.Count, .Team = team.Team}

        Dim e2 = New FileHelperEngine(Of TeamCount)()
        e2.HeaderText = TeamCount.HeaderText
        e2.WriteFile(teamCountFilename, count.ToList)

        Dim lanes = From i In rosterOutList Order By i.Lane, i.EventNumber Group By key = i.Lane Into Group Select Lane = key, Rowers = Group

        Dim e3 = New FileHelperEngine(Of ErgReport)()
        e3.HeaderText = ErgReport.HeaderText
        For Each lane In lanes
            Dim c = From l In lane.Rowers Select New ErgReport With {
                   .Lane = l.Lane,
                   .EventNumber = l.EventNumber,
                   .FirstInitial = l.FirstInitial,
                   .LastName = l.LastName,
                   .Heat = l.Heat,
                   .Team = l.Team
               }
            e3.WriteFile(String.Format(ergReports, lane.Lane), c.ToList)
        Next

        Dim e4 = New FileHelperEngine(Of RaceSchedule)()
        e4.HeaderText = RaceSchedule.HeaderText
        e4.WriteFile(scheduleFilename, From r In schedule Order By r.RaceNumber, r.Heat)



        engine.HeaderText = Roster.HeaderText
        engine.WriteFile(outputFileName, rosterOutList)

        WriteTestRace(racedir)

    End Sub

End Module
