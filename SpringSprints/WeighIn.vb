﻿Imports FileHelpers


'EventNumber	Rank	Team	LastName	FirstInitial

<DelimitedRecord(",")>
<IgnoreFirst()>
Public Class WeighIn

    Public Shared HeaderText = "Team, Event, LastName, FirstInitial"

    Public Team As String

    Public EventName As String

    Public LastName As String

    Public FirstInitial As String

End Class
